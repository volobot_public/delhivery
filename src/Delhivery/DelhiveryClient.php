<?php


namespace volobot\Delhivery;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use volobot\Delhivery\Models\Data\PostalCode;
use volobot\Delhivery\Models\Response\OrderTrackingResponse;
use volobot\Delhivery\Models\Response\PostalCodeResponse;

class DelhiveryClient
{
    /** @var string $token */
    private $token;

    /** @var boolean $staging */
    private $staging;

    /**
     * @var bool
     * Delhivery API returns arrays in increasing order of timestamps.
     * Set the following flag to true to reverse them in the response object
     */
    public static $reverse_scans = false;

    function __construct($token, $staging = true)
    {
        $this->token = $token;
        $this->staging = $staging;
    }

    private function getApiHost(): string
    {
        return $this->staging
            ? "https://staging-express.delhivery.com"
            : "https://track.delhivery.com";
    }

    /**
     * @param string[] $waybill_numbers
     * @return OrderTrackingResponse
     * @throws GuzzleException
     */
    public function trackPackages(array $waybill_numbers): OrderTrackingResponse
    {

        $client = new Client();
        $res = $client->request('GET', $this->getApiHost() . '/api/v1/packages/json/', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
            ],
            'query' => [
                'waybill' => implode(',', $waybill_numbers)
            ]
        ]);

        return new OrderTrackingResponse(json_decode($res->getBody()->getContents(), true));
    }

    /**
     * @param string $postalCode
     * @return PostalCodeResponse
     * @throws GuzzleException
     */
    public function postalCode(string $postalCode): PostalCodeResponse
    {

        $client = new Client();
        $res = $client->request('GET', $this->getApiHost() . '/c/api/pin-codes/json/', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
            ],
            'query' => [
                'filter_codes' => $postalCode
            ]
        ]);

        return new PostalCodeResponse(json_decode($res->getBody()->getContents(), true));
    }

    /**
     * @param string $postalCode
     * @return Models\Data\PostalCodeData|null
     * @throws GuzzleException
     */
    public function singlePostalCode(string $postalCode)
    {
        $api_response = $this->postalCode($postalCode);
        foreach ($api_response->delivery_codes as $delivery_code) {
            if ($delivery_code->postal_code->pin == $postalCode)
                return $delivery_code->postal_code;
        }
        return null;
    }
}