<?php


namespace volobot\Delhivery\Models\Response;


use volobot\Delhivery\Models\Data\PostalCode;

class PostalCodeResponse extends ResponseModel
{
    /** @var PostalCode[] $delivery_codes */
    public $delivery_codes;

    function parseResponse()
    {
        $this->delivery_codes = [];
        $responseData = $this->originalResponse['delivery_codes'] ?? [];
        foreach ($responseData as $response)
        {
            $this->delivery_codes[] = new PostalCode($response);
        }
    }
}