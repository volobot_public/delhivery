<?php


namespace volobot\Delhivery\Models\Response;


abstract class ResponseModel
{
    /** @var array $originalResponse */
    protected $originalResponse;

    /**
     * @param array $originalResponse
     */
    public function __construct(array $originalResponse)
    {
        $this->originalResponse = $originalResponse;
        $this->parseResponse();
    }

    /**
     * @return array
     */
    public function getOriginalResponse()
    {
        return $this->originalResponse;
    }

    abstract function parseResponse();
}