<?php


namespace volobot\Delhivery\Models\Response;


use volobot\Delhivery\Models\Data\ShipmentData;

class OrderTrackingResponse extends ResponseModel
{
    /** @var ShipmentData[] $ShipmentData */
    public $ShipmentData;

    function parseResponse()
    {
        $this->ShipmentData = [];
        $responseData = $this->originalResponse['ShipmentData'] ?? [];
        foreach ($responseData as $response)
            $this->ShipmentData[] = new ShipmentData($response);
    }
}