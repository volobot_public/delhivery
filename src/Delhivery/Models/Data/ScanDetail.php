<?php


namespace volobot\Delhivery\Models\Data;

use Carbon\Carbon;

class ScanDetail extends DataModel
{
    /** @var string $Instructions */
    public $Instructions;

    /** @var string $Scan */
    public $Scan;

    /** @var Carbon $ScanDateTime */
    public $ScanDateTime;

    /** @var string $ScanType */
    public $ScanType;

    /** @var string $ScannedLocation */
    public $ScannedLocation;

    /** @var string $StatusCode */
    public $StatusCode;

    /** @var Carbon $StatusDateTime */
    public $StatusDateTime;

    function fillData(array $data)
    {
        $this->Instructions = $data['Instructions'] ?? "";
        $this->Scan = $data['Scan'] ?? "";
        $this->ScanDateTime = $data['ScanDateTime'] ?? "";
        $this->ScanType = $data['ScanType'] ?? "";
        $this->ScannedLocation = $data['ScannedLocation'] ?? "";
        $this->StatusCode = $data['StatusCode'] ?? "";
        $this->StatusDateTime = $data['StatusDateTime'] ?? "";
    }
}