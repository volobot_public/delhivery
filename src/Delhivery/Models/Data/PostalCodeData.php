<?php


namespace volobot\Delhivery\Models\Data;

class PostalCodeData extends DataModel
{
    /** @var string */
    public $covid_zone;
    
    /** @var int */
    public $pin;
    
    /** @var int */
    public $max_amount;
    
    /** @var string */
    public $pre_paid;
    
    /** @var string */
    public $cash;
    
    /** @var string */
    public $repl;
    
    /** @var string */
    public $cod;
    
    /** @var string */
    public $country_code;
    
    /** @var string */
    public $sort_code;
    
    /** @var string */
    public $is_oda;
    
    /** @var string */
    public $state_code;
    
    /** @var string */
    public $max_weight;

    function fillData(array $data)
    {
        $this->covid_zone = $data['covid_zone'] ?? "";
        $this->pin = $data['pin'] ?? 0;
        $this->max_amount = $data['max_amount'] ?? 0;
        $this->pre_paid = $data['pre_paid'] ?? "";
        $this->cash = $data['cash'] ?? "";
        $this->repl = $data['repl'] ?? "";
        $this->cod = $data['cod'] ?? "";
        $this->country_code = $data['country_code'] ?? "";
        $this->sort_code = $data['sort_code'] ?? "";
        $this->is_oda = $data['is_oda'] ?? "";
        $this->state_code = $data['state_code'] ?? "";
        $this->max_weight = $data['max_weight'] ?? "";
    }
}