<?php


namespace volobot\Delhivery\Models\Data;

class PostalCode extends DataModel
{
    /** @var PostalCodeData */
    public $postal_code;

    function fillData(array $data)
    {
        $this->postal_code = new PostalCodeData($data['postal_code'] ?? []);
    }
}