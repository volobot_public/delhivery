<?php


namespace volobot\Delhivery\Models\Data;


class Status extends DataModel
{
    /** @var string $Instructions */
    public $Instructions;

    /** @var string $RecievedBy */
    public $RecievedBy;

    /** @var string $Status */
    public $Status;

    /** @var string $StatusCode */
    public $StatusCode;

    /** @var string $StatusDateTime */
    public $StatusDateTime;

    /** @var string $StatusLocation */
    public $StatusLocation;

    /** @var string $StatusType */
    public $StatusType;

    function fillData(array $data)
    {
        $this->Instructions = $data['Instructions'] ?? '';
        $this->RecievedBy = $data['RecievedBy'] ?? '';
        $this->Status = $data['Status'] ?? '';
        $this->StatusCode = $data['StatusCode'] ?? '';
        $this->StatusDateTime = $data['StatusDateTime'] ?? '';
        $this->StatusLocation = $data['StatusLocation'] ?? '';
        $this->StatusType = $data['StatusType'] ?? '';
    }
}