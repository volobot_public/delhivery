<?php


namespace volobot\Delhivery\Models\Data;


class Consignee extends DataModel
{
    /** @var string[] $Address1 */
    public $Address1;

    /** @var string[] $Address2 */
    public $Address2;

    /** @var string[] $Address3 */
    public $Address3;

    /** @var string $City */
    public $City;

    /** @var string $Country */
    public $Country;

    /** @var string $Name */
    public $Name;

    /** @var string $PinCode */
    public $PinCode;

    /** @var string $State */
    public $State;

    /** @var string $Telephone1 */
    public $Telephone1;

    /** @var string $Telephone2 */
    public $Telephone2;

    function fillData(array $data)
    {
        $this->Address1 = $data['Address1'] ?? [];
        $this->Address2 = $data['Address2'] ?? [];
        $this->Address3 = $data['Address3'] ?? [];
        $this->City = $data['City'] ?? "";
        $this->Country = $data['Country'] ?? "";
        $this->Name = $data['Name'] ?? "";
        $this->PinCode = $data['PinCode'] ?? "";
        $this->State = $data['State'] ?? "";
        $this->Telephone1 = $data['Telephone1'] ?? "";
        $this->Telephone2 = $data['Telephone2'] ?? "";
    }
}