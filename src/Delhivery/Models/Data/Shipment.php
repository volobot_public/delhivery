<?php


namespace volobot\Delhivery\Models\Data;


use Carbon\Carbon;
use Exception;
use volobot\Delhivery\DelhiveryClient;

class Shipment extends DataModel
{
    /** @var string $AWB */
    public $AWB;

    /** @var double|null $ChargedWeight */
    public $ChargedWeight;

    /** @var double $CODAmount */
    public $CODAmount;

    /** @var Consignee $Consignee */
    public $Consignee;

    /** @var string $Destination */
    public $Destination;

    /** @var double $DispatchCount */
    public $DispatchCount;

    /** @var string $DestRecieveDate */
    public $DestRecieveDate;

    /** @var Carbon|null $FirstAttemptDate */
    public $FirstAttemptDate;

    /** @var double $InvoiceAmount */
    public $InvoiceAmount;

    /** @var string $OrderType */
    public $OrderType;

    /** @var string $Origin */
    public $Origin;

    /** @var Carbon $OriginRecieveDate */
    public $OriginRecieveDate;

    /** @var Carbon $OutDestinationDate */
    public $OutDestinationDate;

    /** @var Carbon $PickUpDate */
    public $PickUpDate;

    /** @var string $ReferenceNo */
    public $ReferenceNo;

    /** @var Carbon $ReturnedDate */
    public $ReturnedDate;

    /** @var boolean $ReverseInTransit */
    public $ReverseInTransit;

    /** @var ScanDetail[] $Scans */
    public $Scans;

    /** @var string $SenderName */
    public $SenderName;

    /** @var Status $Status */
    public $Status;

    function fillData(array $data)
    {
        $this->AWB = $data['AWB'] ?? '';
        $this->ChargedWeight = $data['ChargedWeight'] ?? null;
        $this->CODAmount = $data['CODAmount'] ?? 0;
        $this->Consignee = new Consignee($data['Consignee'] ?? []);
        $this->Destination = $data['Destination'] ?? '';
        try {
            $this->DestRecieveDate = new Carbon($data['DestRecieveDate']);
        } catch (Exception $e) {
            $this->DestRecieveDate = null;
        }
        $this->DispatchCount = $data['DispatchCount'] ?? 0;
        try {
            $this->FirstAttemptDate = new Carbon($data['FirstAttemptDate']);
        } catch (Exception $e) {
            $this->FirstAttemptDate = null;
        }
        $this->InvoiceAmount = $data['InvoiceAmount'] ?? 0;
        $this->OrderType = $data['OrderType'] ?? "";
        $this->Origin = $data['Origin'] ?? "";
        try {
            $this->OriginRecieveDate = new Carbon($data['OriginRecieveDate']);
        } catch (Exception $e) {
            $this->OriginRecieveDate = null;
        }
        try {
            $this->OutDestinationDate = new Carbon($data['OutDestinationDate']);
        } catch (Exception $e) {
            $this->OutDestinationDate = null;
        }
        try {
            $this->PickUpDate = new Carbon($data['PickUpDate']);
        } catch (Exception $e) {
            $this->PickUpDate = null;
        }
        $this->ReferenceNo = $data['ReferenceNo'] ?? "";
        try {
            $this->ReturnedDate = new Carbon($data['ReturnedDate']);
        } catch (Exception $e) {
            $this->ReturnedDate = null;
        }
        $this->ReverseInTransit = $data['ReverseInTransit'] ?? false;
        $this->Scans = [];
        $scans = $data['Scans'] ?? [];
        foreach ($scans as $scan)
        {
            $scanDetail = $scan['ScanDetail'] ?? [];
            $this->Scans[] = new ScanDetail($scanDetail);
        }

        if(DelhiveryClient::$reverse_scans)
        {
            $scans_reversed = array_reverse($this->Scans);
            $this->Scans = $scans_reversed;
        }

        $this->SenderName = $data['SenderName'] ?? "";
        $this->Status = new Status($data['Status'] ?? []);
    }

}