<?php


namespace volobot\Delhivery\Models\Data;


class ShipmentData extends DataModel
{
    /** @var Shipment $Shipment */
    public $Shipment;

    function fillData(array $data)
    {
        $this->Shipment = new Shipment($data['Shipment'] ?? []);
    }
}