<?php


namespace volobot\Delhivery\Models\Data;


abstract class DataModel
{
    function __construct(array $data)
    {
        $this->fillData($data);
        return $this;
    }

    abstract function fillData(array $data);
}